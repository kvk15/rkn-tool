#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from xml.etree import cElementTree as ET

tree = ET.ElementTree(file='dump.xml')
root = tree.getroot()

for child in root:
    if child.text is None:
        print(child.tag, child.attrib) #.decode("unicode_escape")
    else:
        print(child.tag, child.attrib, child.text)

    for c in child:
        if c.text is None:
            print(c.tag, c.attrib)
        else:
            print(c.tag, c.attrib, c.text.encode('utf-8'))
